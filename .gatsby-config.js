const path = require("path");
const pathPrefix = `/foam`;

// Change me
const siteMetadata = {
  title: "A title",
  shortName: "A short name",
  description: "",
  imageUrl: "/graph-visualization.jpg",
  siteUrl: "https://kdbed.gitlab.io",
};
module.exports = {
  siteMetadata,
  pathPrefix,
  flags: {
    DEV_SSR: true,
  },
  plugins: [
    `gatsby-plugin-sharp`,
    {
      resolve: "gatsby-theme-primer-wiki",
      options: {
        defaultColorMode: "night",
        icon: "./path_to/logo.png",
        sidebarComponents: ["tag", "category"],
        nav: [
          {
            title: "Github",
            url: "https://github.com/kdbed/",
          },
          {
            title: "Gitlab",
            url: "https://gitlab.com/kdbed/",
          },
        ],
        editUrl:
          "https://gitlab.com/kdbed/foam/tree/main/",
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "content",
        path: `${__dirname}`,
        ignore: [`**/\.*/**/*`],
      },
    },

    {
      resolve: "gatsby-plugin-manifest",
      options: {
        name: siteMetadata.title,
        short_name: siteMetadata.shortName,
        start_url: pathPrefix,
        background_color: `#f7f0eb`,
        display: `standalone`,
        icon: path.resolve(__dirname, "./path_to/logo.png"),
      },
    },
    {
      resolve: `gatsby-plugin-sitemap`,
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        host: siteMetadata.siteUrl,
        sitemap: `${siteMetadata.siteUrl}/sitemap/sitemap-index.xml`,
        policy: [{ userAgent: "*", allow: "/" }],
      },
    },
  ],
};
